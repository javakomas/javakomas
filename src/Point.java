public class Point {

    final Double x;
    final Double y;

    Point() {
        this.x = 0.0;
        this.y = 0.0;
    }


    Point(String x, String y) {
        this.x = new Double(x);
        this.y = new Double(y);
    }

    Point(Double x, Double y) {
        this.x = x;
        this.y = y;
    }

    Point(Double x) {
        this.x = x;
        this.y = 0.0;
    }

}
