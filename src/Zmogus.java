import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;

public class Zmogus {

    final String name;
    final String lastname;
    String personalCode;
    Float weightKg;
    Short heightCm;
    Boolean hasChildren;
    LocalDate bornDate;
    // F - female, M - male
    Character sex;
    String bloodType;
    String[] transactions;

    final static String[] defaultBloodTypes = {"0", "B-", "AB+", "E"};

    private final static Double cmInM = 100.0;

    Zmogus(
            String name,
            String lastname,
            String personalCode,
            Float weightKg,
            Short heightCm,
            Boolean hasChildren,
            LocalDate bornDate,
            Character sex,
            String bloodType
    ) {
        this.name = name;
        this.lastname = lastname;
        this.personalCode = personalCode;
        this.weightKg = weightKg;
        this.heightCm = heightCm;
        this.hasChildren = hasChildren;
        this.bornDate = bornDate;
        this.sex = sex;
        this.bloodType = bloodType;
    }

    Double kmi() {
        Double heightM = heightCm / cmInM;
        System.out.println(heightM);

        Double kmi = weightKg / (heightM * heightM);
        BigDecimal bd = new BigDecimal(kmi);
        bd = bd.setScale(4, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    static String[] defaultBloodTypes() {
        return defaultBloodTypes;
    }

    Integer age() {
        return LocalDate.now().getYear() - bornDate.getYear();
    }

    String initials() {
        return name.substring(0, 1) + lastname.substring(0, 1);
    }
}
