public class Uzduotys2 {

    /*
    Duoti trys skaičiai: a, b, c. Nustatykite ar šie skaičiai gali būti
    trikampio kraštinių ilgiai ir jei gali tai kokio trikampio:
    lygiakraščio, lygiašonio ar įvairiakraščio. Atspausdinkite
    atsakymą. Kaip pradinius duomenis panaudokite tokius skaičius:
    3, 4, 5
    2, 10, 8
    5, 6, 5
    5, 5, 5
     */
    void task1() {

        Integer[][] triangles = {
                {
                    3, 4, 5
                },
                {
                    10, 2, 8
                },
                {
                    5, 6, 5
                }
        };

        // input these values using scanner;


        for (Integer[] triangleSides: triangles) {
            Integer a = triangleSides[0];
            Integer b = triangleSides[1];
            Integer c = triangleSides[2];
            String result = checkTriangleType(a, b, c);
            System.out.println(result);
        }
    }

    String checkTriangleType(Integer a, Integer b, Integer c) {
        if (a > b) return "OK"; else return "Bad";
    }

    /*
    Apskaiciuoti trikampiu plotus
    3, 4, 5
    2, 10, 8
    5, 6, 5
    5, 5, 5
     */
    void task2() {

    }

    // duotas sveikuju skaiciu masyvas {-5, 4, 10, -4, 0, 7, -8, 9, 8, 15}
    // surasti min ir max
    // negalima naudoti min, max;
    void task3() {

    }

    // duotas sveikuju skaiciu masyvas {-5, 4, 10, -4, 0, 7, -8, 9, 8, 15}
    // isrikiuoti didejimo tvarka
    // negalima naudoti Collections.sort(array[Int]);
    void task4() {

    }
}
