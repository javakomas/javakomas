package com.baltictalents.lessons.polymorphism;

public interface Readable {

    Integer[] getArray();
}
