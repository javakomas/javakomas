package com.baltictalents.tasks;

import java.util.Scanner;

public class Tasks1 {

    // programa papraso ivesti skaiciu, i ekrana reikia ivesti ar skaicius lyginis ar nelyginis
    void task1() {


        Double a = readDouble("a");
        Double b = readDouble("b");
        Double c = readDouble("c");

    }

    Double readDouble(String paramName) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Input digit " + paramName + " = ");
        Double a = scanner.nextDouble();
        return a;
    }

    // programa papraso ivesti skaiciu, i ekrana isvedama skaiciaus saknis
    void task2() {

    }

    // programa papraso ivesti reiksmes a,b,c, kurios yra kvadratines lygties ax*x+bx+c reiksmes,
    // i ekrana isvedama kvadratines lygties sprendimas x1 = ?, x2 = ?
    void task3() {

    }

    // programa papraso ivesti reiksmes a,b,c, kurios yra kvadratines lygties ax*x+bx+c reiksmes,
    // i ekrana isvedam kvadratines lygties reiksmes intervale [-5, 5]
    void task4() {

    }

    String oddOrEven(Double a) {
        String result = "";
        if (a % 2 == 0) {
            result = "Even";
        } else {
            result = "Odd";
        }
        return result;
    }

    XTuple solveSquareEquation(Double a, Double b, Double c) {

        Double bRoot = b * b;
        Double d = bRoot - 4 * a * c;
        XTuple result = new XTuple();
        if (d >= 0) {
            Double dRoot = Math.sqrt(d);
            result.x1 = x(a, b, dRoot);
            result.x2 = d == 0 ? null : x(a, b, -dRoot);
        } else {
            result.x1 = null;
            result.x2 = null;
        }
        return result;
    }

    Double x(Double a, Double b, Double dRoot) {
        return (-b + dRoot) / (2 * a);
    }

    class XTuple {
        Double x1;
        Double x2;
    }
}
